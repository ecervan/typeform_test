# Typeform test
In this repo you can find the code for challenge 1 (completion rate) and 2 (question clustering) of the typeform test. 

### Installation
Each subfolder has its own installation instructions.

### Analysis document

Here you can find a document with the analysis and considerations taken for each challenge:
[Analysis document](https://docs.google.com/document/d/1UoZaDDT5w6VLjZ-v_ohplEQzF7ztkyeePEeiWduUEPk/edit?usp=sharing)
# Unsupervised clustering of form questions
This Code fine tunes a bert model on form_questions dataset and extracts clustered sentences

### Installation
To be able to run this code create a new virtual environment and run:
```sh
$ cd challenge_2
$ pip install -r requirements.txt
```

To be able to use the GPU you will need CUDA 10.2. Otherwise, it will use the CPU. 

### Training

For running the full pipeline (clean/split data, train, extract/cluster) run:
```sh
$ make run-pipeline
```

Tu run independent steps:

Pre-process data
```sh
$ make preprocess-data
```
Train bert model
```sh
$ make train
```
Extract and cluster sentences
```sh
$ make cluster
```
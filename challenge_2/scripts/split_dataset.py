"""
Script for cleaning and splitting dataset
"""

import os
import pandas

from sklearn.model_selection import train_test_split


def clean_data(data):
    """
    Remove unwanted characters
    """
    data = data.str.replace(r"[\"\',]", '')
    data = data.str.replace('</strong>', '')
    data = data.str.replace('<strong>', '')
    data = data.str.replace('<br />', '')
    data = data.str.replace('<br>', '')
    data = data.str.replace('<em>', '')
    data = data.str.replace('[', '')
    data = data.str.replace(']', '')
    data = data.dropna()
    return data

def process_data(data_path, output_path):
    """
    Read, clean, split and store data
    """
    # Read
    data = pandas.read_csv(data_path, header=None)

    # Clean
    data[0] = clean_data(data[0])

    # Split
    X_train, X_test = train_test_split(
        data, test_size=0.3, shuffle=True)
    output_train_path = os.path.join(output_path, 'form_questions_train.csv')
    output_test_path = os.path.join(output_path, 'form_questions_test.csv')

    # Store
    X_train.to_csv(output_train_path, index=False, header=False)
    X_test.to_csv(output_test_path, index=False, header=False)


if __name__ == "__main__":
    data_path = './form_questions.csv'
    output_path = './dataset/'

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    process_data(data_path, output_path)

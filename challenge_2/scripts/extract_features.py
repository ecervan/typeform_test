"""
Script for extracting features and clustering them
"""

import os
import pandas
import numpy as np
import torch
from tqdm import tqdm

from transformers import LineByLineTextDataset, \
                         BertForMaskedLM, \
                         BertTokenizerFast, \
                         pipeline
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
import seaborn as sns
from matplotlib import pyplot


def get_test_dataset(dataset_path):
    """
    Load dataset
    """

    dataset = pandas.read_csv(dataset_path, header=None)
    dataset = dataset[0].values.tolist()

    return dataset

def get_pipeline(model_path, base_model_name):
    """
    Load fine tuned model and get base model tokenizer
    """

    if torch.cuda.is_available():
        device = torch.cuda.current_device()
    else:
        device = -1 # Use CPU

    tokenizer = BertTokenizerFast.from_pretrained(base_model_name)
    extract_features = pipeline(
        'feature-extraction',
        model=model_path,
        tokenizer=tokenizer,
        device=device
    )

    return extract_features

def extract_features(pipeline, dataset, batch_size):
    """
    Extract last layer features for each token and fuse them
    """

    # Extract sentence features
    sentences_features = []
    batch_dataset = [dataset[i:i+batch_size] for i in range(0, len(dataset), batch_size)]
    for batch_data in tqdm(batch_dataset):
        features = pipeline(batch_data)
        np_features = np.array(features)
        sentence_features = np.mean(np_features, axis=1)
        sentences_features.append(sentence_features)

    np_sentences_features = np.concatenate(sentences_features, axis=0)

    return np_sentences_features

def extract_clusters(features, num_clusters, output_cluster_path, dataset):
    """
    Use k-means to extract clusters and save the related sentences
    """

    # Cluster sentences
    n_clusters = range(1, 15)
    estimators = [KMeans(n_clusters=i) for i in n_clusters]
    score = [estimators[i].fit(features).score(features) for i in range(len(n_clusters))]
    pyplot.plot(n_clusters, score)

    estimator = KMeans(n_clusters=num_clusters)
    clusters = estimator.fit_predict(features)

    np_dataset = np.array(dataset)
    for cluster_id in range(num_clusters):
        indices = np.where(clusters==cluster_id)[0]
        grouped_sentences = np_dataset[indices]
        cluster_path = os.path.join(output_cluster_path, f"cluster_{cluster_id}.csv")
        np.savetxt(cluster_path, grouped_sentences, delimiter=",", fmt = '%s')

    # t-sne visualization
    tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=500)
    tsne_results = tsne.fit_transform(features)
    data = pandas.DataFrame(tsne_results, columns = ['tsne1', 'tsne2'])
    data['clusters'] = clusters
    pyplot.figure(figsize=(16,10))
    sns.scatterplot(
        x="tsne1", 
        y="tsne2",
        hue="clusters",
        palette=sns.color_palette("hls", num_clusters),
        data=data,
        legend="full",
        alpha=0.3
    )
    pyplot.show()


if __name__ == "__main__":
    test_dataset = './dataset/form_questions_test.csv'
    output_model_path = './finetuned_bert'
    base_model_name = 'bert-large-uncased'
    output_cluster_path = './clusters/'
    num_clusters = 8
    batch_size = 1

    if not os.path.exists(output_cluster_path):
        os.makedirs(output_cluster_path)

    print("Loading data")
    test_dataset = get_test_dataset(test_dataset)
    print("Obtain pipeline")
    pipeline = get_pipeline(output_model_path, base_model_name)
    print("Extract features")
    features = extract_features(pipeline, test_dataset, batch_size)
    print("Compute Clusters and Visualizations")
    extract_clusters(features, num_clusters, output_cluster_path, test_dataset)
"""
Script for training the model 
"""

import numpy as np

from transformers import (
    LineByLineTextDataset, 
    BertForMaskedLM, 
    BertTokenizerFast, 
    DataCollatorForLanguageModeling, 
    Trainer, 
    TrainingArguments 
)

def get_pretrained_model(model_name):
    """
    Load pretrained model and tokenizer
    """

    # Load tokenizer
    tokenizer = BertTokenizerFast.from_pretrained(model_name)

    # Load pretrained model
    model = BertForMaskedLM.from_pretrained(model_name)

    return model, tokenizer

def get_training_dataset(datset_path):
    """
    Read a dataset we can train on
    """

    # Load dataset
    dataset = LineByLineTextDataset(
                    tokenizer=tokenizer,
                    file_path=datset_path,
                    block_size=128,
                )

    return dataset

def train(model, tokenizer, results_path, logs_path, output_model_path):
    """
    Prepare trainer and add masking of words for the target masking task
    """

    # Create collator to mask words
    data_collator = DataCollatorForLanguageModeling(
        tokenizer=tokenizer, mlm=True, mlm_probability=0.20
    )

    # Define training parameters
    training_args = TrainingArguments(
        output_dir=results_path,
        num_train_epochs=5,
        per_device_train_batch_size=8,
        per_device_eval_batch_size=8,
        warmup_steps=400,
        weight_decay=0.01,
        logging_dir=logs_path,
    )

    # Create trainer
    trainer = Trainer(
        model=model,
        args=training_args,
        data_collator=data_collator,
        train_dataset=dataset,
        prediction_loss_only=True,
    )

    # Train the model
    trainer.train()
    trainer.save_model(output_model_path)

if __name__ == "__main__":
    results_path = './results'
    logs_path = './logs'
    train_dataset_path = './dataset/form_questions_train.csv'
    model_name = 'bert-large-uncased'
    output_model_path = './finetuned_bert'

    model, tokenizer = get_pretrained_model(model_name)
    dataset = get_training_dataset(train_dataset_path)
    train(model, tokenizer, results_path, logs_path, output_model_path)



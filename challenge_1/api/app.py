from flask import Flask, request, jsonify
import numpy as np
import xgboost as xgb

app = Flask(__name__)


@app.route('/api/predict_completion_rate/', methods=['POST'])
def predict_completion_rate():
    """
    Api endpoint that gets batches of features and returns a batch of predicted
    completion rates
    """
    data = request.get_json()
    input_data = np.array(data)
    prediction = model.predict(input_data)
    return jsonify(prediction.tolist())

if __name__ == '__main__':
    model_path = './models/completion_rate.model'
    model = xgb.XGBRegressor()
    model.load_model(model_path)
    app.run(host='0.0.0.0')
# Completion Rate
This code analyzes, trains an XGBoost model and evaluates the results on the completion_rate dataset. Also once the model is trained you can use an API to serve predictions.

### Installation
To be able to run this code create a virtual environment and run:
```sh
$ cd challenge_1
$ pip install -r requirements.txt
```

### Training
For just training the model run:
```sh
$ make train
```
If you want to see intermediate plots run:
```sh
$ make train-verbose
```

### API
Once the model is trained you can start serving predictions.
To start the server run:
```sh
$ make run-api
```
If you want to test the api is working run on another terminal:
```sh
$ make test-api
```

"""
Train and evaluate an XGBoost model for completion rate predictions
"""

import argparse
import logging
import math
import os
import pandas

import numpy as np
import seaborn as sns
import xgboost as xgb

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import KFold
from xgboost import plot_importance
from matplotlib import pyplot


logger = logging.getLogger(__name__)

SEED = 0

def preprocess_data(data_path, verbose=False):
    """
    Load data from csv and prepare splits for later training and evaluation
    """

    logger.info("Processing data")

    # Loading data
    data = pandas.read_csv(data_path)

    # Data analysis
    # Check correlated features and identify duplicates
    if verbose:
        pyplot.figure(figsize=(15,8))
        corr = data.loc[:, 'feat_01':].corr()
        mask = np.zeros_like(corr, dtype=np.bool)
        mask[np.triu_indices_from(mask)] = True
        sns.heatmap(
            corr, linewidths=.5,annot=True,mask=mask,cmap='coolwarm')

        pyplot.title('Feature correlation')
        pyplot.show()

    # Remove duplicated features 
    del data['feat_34']
    del data['feat_36']

    # Create train test validation split
    data_X = data.loc[:, 'feat_01':]
    data_Y = data['submissions'] / data['views']
    X_train, X_test, Y_train, Y_test = train_test_split(
        data_X, data_Y, test_size=0.1, shuffle=True, random_state=SEED)
    X_train, X_val, Y_train, Y_val = train_test_split(
        X_train, Y_train, test_size=0.2, shuffle=True, random_state=SEED)

    return (X_train, Y_train), (X_val, Y_val), (X_test, Y_test)

def train(train_data, val_data, model_path, train_config, verbose=False):
    """
    Prepare model parameters and do cross validation with random search to fit 
    the model.
    """

    logger.info("Fitting the model")

    # Model parameters
    params = {
        'n_estimators': np.arange(600, 1400, 50),
        'learning_rate': np.arange(0.01,0.1,0.005),
        'max_depth': np.arange(4, 10, 1),
        'subsample':  np.arange(0.6, 1, 0.1),
        'colsample_bytree': np.arange(0.6, 1, 0.1),
        'objective':['reg:squarederror'],
        'tree_method': ['gpu_hist']
    }

    eval_set = [train_data, val_data]

    #Training parameters
    fit_params = {
        'eval_metric': 'rmse', 
        'eval_set': eval_set, 
        'verbose':verbose, 
        'early_stopping_rounds':train_config.get('early_stop_iters', 120)
    }
    X_train, Y_train = train_data
    n_splits = train_config.get('splits', 5)
    iters = train_config.get('iterations', 200)

    # Create model and do Random search with cross validation to find the 
    # optimal set of parameters. 
    model = xgb.XGBRegressor(**params)
    kfold = KFold(n_splits=n_splits, shuffle=True, random_state=10)
    grid_search = RandomizedSearchCV(
        model, params, scoring="neg_mean_squared_error", n_iter=iters, cv=kfold)
    grid_result = grid_search.fit(X_train, Y_train, **fit_params)

    # Get the best model and save it 
    model = grid_result.best_estimator_
    model.save_model(model_path)

    return model

def evaluate(model, test_data, verbose=False):
    """
    Evaluate selected model
    """

    logger.info("Model Evaluation")

    # Plot train and validation curves of the selected model 
    results = model.evals_result()
    epochs = len(results['validation_0']['rmse'])
    if verbose:
        x_axis = range(0, epochs)
        fig, ax = pyplot.subplots()
        ax.plot(x_axis, results['validation_0']['rmse'], label='Train')
        ax.plot(x_axis, results['validation_1']['rmse'], label='Val')
        ax.legend()
        pyplot.ylabel('RMSE')
        pyplot.title('XGBoost RMSE')
        pyplot.show()

    # Get predictions on the test set
    X_test, Y_test = test_data
    predictions = model.predict(X_test)

    if verbose:
        # Visualize some predictions predictions
        pyplot.figure(figsize=(20,10))
        pyplot.plot(predictions[0:120], linewidth=2)
        pyplot.plot(Y_test.to_numpy()[0:120], linewidth=2)
        pyplot.title('Predictions')
        pyplot.show()

        # Plot gain feature importance 
        plot_importance(model, importance_type='gain')
        pyplot.show()

    # Compute rmse in test set
    rmse = math.sqrt(mean_squared_error(predictions, Y_test))
    logger.info(f"Test mean squared error is: {rmse}")

    return rmse 

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', dest='verbose', action='store_true')
    arguments = parser.parse_args()

    verbose = arguments.verbose

    # Define config 
    model_root_path = './models'
    if not os.path.exists(model_root_path):
        os.makedirs(model_root_path)

    data_path = './completion_rate.csv'
    model_path = './models/completion_rate.model'

    logging.basicConfig(
        level=logging.INFO if verbose else logging.WARN,
    )

    train_config = {
        'splits': 2,
        'iterations': 2,
        'early_stop_iters': 10
    }

    # Execute training evaluation steps
    train_data, val_data, test_data = preprocess_data(data_path, verbose)
    model = train(train_data, val_data, model_path, train_config, verbose)
    rmse = evaluate(model, test_data, verbose)

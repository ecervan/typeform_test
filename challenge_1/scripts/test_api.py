import requests
import json
import numpy as np

# Dummy set of features (the model takes 45 features as input)
features = [[0,0,0,0,1,0,4,0,0,0,0,0,0,0,0,0,1,0,0,0,6,0,4,0,0,0,4,1,4,0,0,0,0,0,0,0,0,0,0,1,1,1,11,7,21],
	    	[1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,3,1,2,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2]]

# Prepare request
url = 'http://0.0.0.0:5000/api/predict_completion_rate/'
json_feats = json.dumps(features)
headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
response = requests.post(url, data=json_feats, headers=headers)

# Process response
predictions = json.loads(response.text)
if response.status_code == 200:
	print(f"Succesful request! Predicted values are: {predictions}")
else:
	print(f"Error! Request status: {response.status_code}")